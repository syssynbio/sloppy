CARBON_SOURCES_LIST = [
    ('glc__D', ),
    ('fru', ),
    ('6pgc', ),
    ('r5p', ),
    ('succ', ),
    ('xu5p__D', ),
    ('2pg', ),
    ('ac', ),
    ('dhap', ),
]

SINGLE_KOS = [
    'GLCpts',
    'PGI',
    'PFK',
    'FBP',
    'FBA',
    'TPI',
    'GAPD|PGK',
    'PGM',
    'ENO',
    'PYK',
    'PPS',
    'PDH',
    'PFL',
    'G6PDH2r|PGL',
    'GND',
    'EDD|EDA',
    'RPE',
    'RPI',
    'TKT1|TKT2',
    'TALA',
    'PPC',
    'PPCK',
    'ICL',
    'MALS',
    'CS',
    'ACONTa|ACONTb',
    'ALCD2x',
    'ACALD',
]

TARGET_REACTION = 'RBC'

WILDTYPE_MODEL = "core_model_with_rpp.xml"
