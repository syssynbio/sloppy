# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


CARBON_SOURCES_LIST = [
    ('glycerate', ),
    ('glycerate' ,'glycerol'),
    ('glycerate' ,'glycerol' ,'glycine'),
    ('glycerate', 'succinate'),
    ('glycerate', 'glycerol', 'succinate'),
    ('glycerate', 'glycerol' ,'succinate', 'glycine'),
]

SINGLE_KOS = [
    'G6PDH2r', # coupled to ΔPGL ΔGND
    'TALA',
    'RPE',
    'RPI',
    'PGI',
    'PFK',
    'FBP',
    'FBA',
    'TPI',
    'PGK', # coupled to ΔGAPD
    'PGM',
    'ENO',
    'PYK',
    'PPS',
    'PDH',
    'PPC',
    'PPCK',
    'ME1|ME2',
    'CS', # coupled to ΔACONTa ΔACONTb
    'ICDHyr',
    'ICL', # coupled to ΔMALS
    'AKGDH', # coupled to ΔSUCOAS
    'SUCDi',
    'FRD7',
    'FUM',
    'MDH',
    'SER_ABC',
    'glyA',
    'GCV',
]

TARGET_REACTION = "glycerate_t"

WILDTYPE_MODEL = "core_model_extended.xml"
