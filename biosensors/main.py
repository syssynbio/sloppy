import biosensors
import pandas as pd
from cobra.io import read_sbml_model
from tqdm import tqdm
from importlib_resources import read_text

from optslope import calculate_slope_multi, filter_redundant_knockouts

def main(max_ko: int = 4, num_processes: int = 1) -> None:
    FULL_RESULT_PATH = f"biosensors/results_full_{max_ko}KOs.csv"
    SUMMARY_PATH = f"biosensors/results_summary_{max_ko}KOs.csv"

    wt_model = read_sbml_model(read_text(biosensors, biosensors.WILDTYPE_MODEL))

    dfs = []
    print(f"Calculating slopes for up to {max_ko} knockouts, and "
          f"for {len(biosensors.CARBON_SOURCES_LIST)} carbon source combinations")

    for carbon_sources in tqdm(biosensors.CARBON_SOURCES_LIST,
                               total=len(biosensors.CARBON_SOURCES_LIST),
                               desc="Carbon Sources"):
        df = calculate_slope_multi(
            wt_model=wt_model,
            carbon_sources=carbon_sources,
            single_knockouts=biosensors.SINGLE_KOS,
            target_reaction=biosensors.TARGET_REACTION,
            max_knockouts=max_ko,
            num_processes=num_processes,
            chunksize=10)

        dfs.append(df)

    result_df = pd.concat(dfs)
    result_df.carbon_sources = result_df.carbon_sources.str.join(' + ')
    result_df = result_df.round(3).fillna(-1.0)

    # write all the slopes to a CSV file
    with open(FULL_RESULT_PATH, 'w') as fp:
        result_df.to_csv(fp)

    pivot_df = filter_redundant_knockouts(result_df)

    # reorder the columns to match the original list order
    col_order = list(map(' + '.join, biosensors.CARBON_SOURCES_LIST)) + [
        'smallest_slope', 'highest_slope', 'slope_ratio', 'no_knockouts']
    pivot_df = pivot_df[col_order]

    # convert the list of knockouts into a simple string
    pivot_df.index = pivot_df.index.str.join("|")

    # write summary of results to CSV file
    with open(SUMMARY_PATH, "w") as fp:
        pivot_df.round(3).to_csv(fp)
