# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import logging
import os
import click
import click_log

logger = logging.getLogger()
click_log.basic_config(logger)

try:
    DEFAULT_NUM_PROCESSES = max(1, len(os.sched_getaffinity(0)) - 1)
except OSError:
    logger.warning(
        "Could not determine the number of cores available - assuming 1."
    )
    DEFAULT_NUM_PROCESSES = 1

@click.group()
@click.help_option("--help", "-h")
@click_log.simple_verbosity_option(
    logger,
    default="INFO",
    show_default=True,
    type=click.Choice(["CRITICAL", "ERROR", "WARN", "INFO", "DEBUG"]),
)
def cli():
    """
    Command line interface
    """
    pass


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "--max_ko",
    metavar="MAX_KO",
    default=5,
    show_default=True,
    help="Maximum number of knockouts to consider for each mutant",
)
@click.option(
    "--num_processes",
    metavar="NUM_PROCESSES",
    default=DEFAULT_NUM_PROCESSES,
    show_default=True,
    help="number of processes to use in parallel (should be roughly no. of "
         "CPU cores)",
)
@click.option(
    "--no_ed",
    metavar="NO_ED",
    default=False,
    show_default=True,
    help="If True, the Entner-Doudoroff pathway will be knockout out in all "
         "considered mutants",
)
def methanol(max_ko: int, num_processes: int, no_ed: bool):
    """Calculate a DataFrame with slopes of all KOs on all CSs."""
    from methanol.main import main
    main(max_ko, num_processes, no_ed)


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "--num_processes",
    metavar="NUM_PROCESSES",
    default=DEFAULT_NUM_PROCESSES,
    show_default=True,
    help="number of processes to use in parallel (should be roughly no. of "
         "CPU cores)",
)
@click.option(
    "--max_ko",
    metavar="MAX_KO",
    default=4,
    show_default=True,
    help="Maximum number of knockouts to consider for each mutant",
)
def biosensors(max_ko: int, num_processes: int):
    """Calculate a DataFrame with slopes of all KOs on all CSs."""
    from biosensors.main import main
    main(max_ko, num_processes)


@cli.command(help="This script recreates a 2D heatmap of all double knockouts "
                  "and their slopes, as shown in figure S3(a) in Antonovsky "
                  "et al. (Cell 2016)")
@click.help_option("--help", "-h")
@click.option(
    "--num_processes",
    metavar="NUM_PROCESSES",
    default=DEFAULT_NUM_PROCESSES,
    show_default=True,
    help="number of processes to use in parallel (should be roughly no. of "
         "CPU cores)",
)
def rubisco(num_processes: int):
    """Calculate a DataFrame with slopes of all KOs on all CSs."""
    from rubisco.main import main
    main(num_processes)


if __name__ == "__main__":
    cli()
