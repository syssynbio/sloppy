Sloppy
======

A collection of projects that use [OptSlope](https://gitlab.com/elad.noor/optslope).
To run the scripts in this repository, one must first install the `optslope` package.
The easiest option is PyPI:
```
pip install optslope
```

Otherwise, to install from source:
```
git clone https://gitlab.com/elad.noor/optslope.git
cd optslope
python setup.py install
```

# RuBisCO dependent E. coli
This algorithm was used by [Antonovsky et al.](https://www.cell.com/fulltext/S0092-8674(16)30668-7)
to find knockout combinations that couple *E. coli* growth to the flux in RuBisCO.

# Methanol dependent E. coli
Similarly, this algorithm was used by [Meyer et al.](https://www.nature.com/articles/s41467-018-03937-y)
to design knockouts that couple the growth of *E. coli* to methanol, as an additional
carbon source on top of more complex carbohydrates.

# E. coli as a glycerate biosensor
A recent project conducted by Selcuk et al, used OptSlope to design knock-out
combinations that have varying dependencies on glycerate in the media.

# Other references
A recent publication by [Jensen et al.](https://www.sciencedirect.com/science/article/pii/S2214030118300373)
uses a similar approach for strain design


