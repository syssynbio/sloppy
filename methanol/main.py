import methanol
import pandas as pd
from cobra.io import read_sbml_model
from tqdm import tqdm
from importlib_resources import read_text

from optslope import calculate_slope_multi, filter_redundant_knockouts

def main(max_ko: int = 5, num_processes: int = 1, no_ed: bool = False) -> None:
    """Calculate a DataFrame with slopes of all KOs on all CSs."""
    FULL_RESULT_PATH = f"methanol/results_full_{max_ko}KOs.csv"
    SUMMARY_PATH = f"methanol/results_summary_{max_ko}KOs.csv"
    wt_model = read_sbml_model(read_text(methanol, methanol.WILDTYPE_MODEL))

    if no_ed:
        wt_model.reactions.EDD.knock_out()
        wt_model.reactions.EDA.knock_out()
        FULL_RESULT_PATH = f"methanol/results_full_noed_{max_ko}KOs.csv"
        SUMMARY_PATH = f"methanol/results_summary_noed_{max_ko}KOs.csv"
        single_kos = methanol.SINGLE_KOS
    else:
        single_kos = methanol.SINGLE_KOS + ["EDD|EDA"]

    dfs = []
    print(f"Calculating slopes for up to {max_ko} knockouts, and "
          f"for {len(methanol.CARBON_SOURCES_LIST)} carbon source combinations")

    for carbon_sources in tqdm(methanol.CARBON_SOURCES_LIST,
                               total=len(methanol.CARBON_SOURCES_LIST),
                               desc="Carbon Sources"):
        df = calculate_slope_multi(
            wt_model=wt_model,
            carbon_sources=carbon_sources,
            single_knockouts=single_kos,
            target_reaction=methanol.TARGET_REACTION,
            max_knockouts=max_ko,
            num_processes=num_processes,
            chunksize=10)

        dfs.append(df)

    result_df = pd.concat(dfs)
    result_df.carbon_sources = result_df.carbon_sources.str.join(' + ')
    result_df = result_df.round(3).fillna(-1.0)

    # write all the slopes to a CSV file
    with open(FULL_RESULT_PATH, "w") as fp:
        result_df.to_csv(fp)

    pivot_df = filter_redundant_knockouts(result_df)

    # reorder the columns to match the original list order
    col_order = list(map(' + '.join, methanol.CARBON_SOURCES_LIST)) + [
        'smallest_slope', 'highest_slope', 'slope_ratio', 'no_knockouts']
    pivot_df = pivot_df[col_order]

    # convert the list of knockouts into a simple string
    pivot_df.index = pivot_df.index.str.join("|")

    # write summary of results to CSV file
    with open(SUMMARY_PATH, "w") as fp:
        pivot_df.to_csv(fp)
