CARBON_SOURCES_LIST = [
    ('methanol', 'glc__D', ),
    ('methanol', 'fru', ),
    ('methanol', '6pgc', ),
    ('methanol', 'r5p', ),
    ('methanol', 'succ', ),
    ('methanol', 'xu5p__D', ),
    ('methanol', '2pg', ),
    ('methanol', 'ac', ),
    ('methanol', 'dhap', ),
    ('methanol', ),
]

SINGLE_KOS = [
    'GLPX',
    'GLCpts',
    'PGI',
    'PFK|PFK2',
    'FBP',
    'FBA|FBA2',
    'TPI',
    'PGK', # coupled to ΔGAPD
    'PGM',
    'ENO',
    'PYK',
    'PPS',
    'PDH',
    'PFL',
    'G6PDH2r',
    'PGL', # coupled to ΔGND
    'RPE',
    'RPI',
    'TKT1|TKT2',
    'TALA',
    'PPC',
    'PPCK',
    'ME1|ME2',
    'CS', # coupled to ΔACONTa ΔACONTb
    'ICDHyr',
    'ICL', # coupled to ΔMALS
    'AKGDH', # coupled to ΔSUCOAS
    'SUCDi',
    'FRD7',
    'FUM',
    'MDH',
    'ALCD2x',
    'ACALD']

TARGET_REACTION = 'H6PS'

WILDTYPE_MODEL = "core_model_with_rump.xml"
